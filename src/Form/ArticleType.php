<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [

                'label' => "Titre de mon article",
                'attr' => [
                    'placeholder' => 'Titre',
                    'required' => true
                ]
            ])
            ->add(
                'content',
                TextareaType::class,
                [
                    'label' => "Contenu de mon article qui doit les <a href=''>reco</a>",
                    'attr' => [
                        'required' => true
                    ]
                ]
            )
            ->add(
                'image',
                TextType::class,
                [
                    'label' => "Image",
                    'attr' => [
                        'placeholder' => 'nom de l\'image',
                        'class' => 'form-control',
                    ]
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
