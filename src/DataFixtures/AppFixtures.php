<?php

namespace App\DataFixtures;


use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $cat1 = new Category();
        $cat1->setTitle("Informatique");
        $cat1->setDescription("Vos news sur l'informatique");
        $manager->persist($cat1);

        $cat2 = new Category();
        $cat2->setTitle("Politique");
        $cat2->setDescription("Vos news sur l'arene politique");
        $manager->persist($cat2);

        $cat3 = new Category();
        $cat3->setTitle("Jeux Video");
        $cat3->setDescription("pour les Geeks");
        $manager->persist($cat3);

        $cats = [$cat1, $cat2, $cat3];
        
        $com = new Comment();
        $com->setAuthor("Alan Smithy");
        $com->setContent("C'est cool");
        $com->setCreatedAt(new \DateTime());
        $com->setUpdatedAt(new \DateTime());

        $com1 = new Comment();
        $com1->setAuthor("Bono");
        $com1->setContent("I'rt Rock");
        $com1->setCreatedAt(new \DateTime());
        $com1->setUpdatedAt(new \DateTime());

        $com2 = new Comment();
        $com2->setAuthor("Neerds du 50");
        $com2->setContent("Ca rocks du ponay");
        $com2->setCreatedAt(new \DateTime());
        $com2->setUpdatedAt(new \DateTime());
        
        $coms = [$com, $com1, $com2];
        $arts = [];

        for ($i = 1; $i <= 3; $i++) {
            
            $article1 = new Article();
            $article1->setTitle("Article " . $i);
            $article1->setContent("<p>Quis nulla provident exercitationem minima id, ab eveniet qui illo error eligendi, eaque adipisci doloribus.
        </p>
        <p>Unde laborum quaerat qui, voluptatem placeat sunt provident, excepturi eveniet et sit, itaque molestias
            laudantium!</p>");
            $article1->setImage("http://placehold.it/350x150");
            $article1->setCreatedAt(new \DateTime());
            $article1->setUpdatedAt(new \DateTime());
            $article1->setCategory($cats[mt_rand(0,2)]);

            $manager->persist($article1);
            $arts[] = $article1;
        }

        // je boucle sur les commentaires
        foreach ($coms as $key => $com) {
            // pour chaque commentaire, je les attribue de maniere aléatoire sur les articles
            $com->setArticle($arts[mt_rand(0,2)]);
            $manager->persist($com);
        }

        $manager->flush();


    }

}
