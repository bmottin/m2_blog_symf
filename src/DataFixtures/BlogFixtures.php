<?php

namespace App\DataFixtures;

use App\Entity\Blog;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class BlogFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $blog = new Blog();
        $blog->setSlogan("Le blog qui les bien");
        $blog->setContactEmail("ben@blog.com");
        $blog->setCgu("C'est mes Cgu de mon blog
        
        <h2>Article 1</h2>
        <p>J'accepte</p>
        ");
        $manager->persist($blog);
        $manager->flush();
    }
}
