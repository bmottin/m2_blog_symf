<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Repository\BlogRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /** 
     * @Route("/", name="home")
     */
    public function home(BlogRepository $blogRepository): Response
    {
        $blog = $blogRepository->findAll();
        dump($blog);
        return $this->render('blog/home.html.twig', [
            'accueil' => 'Accueil - bienvenue',
            'slogan' => $blog[0]->getSlogan(),
            'age' => 18
        ]);
    }

    /** 
     * @Route("/cgu", name="cgu")
     * avec injection de dépendance
     */
    public function cgu(BlogRepository $blogRepo): Response
    {
        /*
            is_tldr => false
            si tldr j'affiche juste le "Trop long pas lu"
            sinon "ceci est les CGU"
        */
        // $blogRepo = $this->getDoctrine()->getRepository(Blog::class);
        $blog = $blogRepo->findOneBy(['id' => 1]);
        dump($blog);
        return $this->render('blog/cgu.html.twig', [
            'cgu_content' => $blog->getCgu(),
            'is_tldr' => false,
            
        ]);
    }
}
