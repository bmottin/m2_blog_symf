<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article")
     */
    public function index(): Response
    {
        $articleRepo = $this->getDoctrine()->getRepository(Article::class);
        $articles = $articleRepo->findAll();
        return $this->render('article/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/article/{id}/show", name="article_show")
     */
    public function show($id): Response
    {
        $articleRepo = $this->getDoctrine()->getRepository(Article::class);
        $article = $articleRepo->findOneBy(['id' => $id]);

        return $this->render('article/show.html.twig', [
            'article' => $article
        ]);
    }

    /**
     * @Route("/article/new", name="article_new")
     */
    public function new(Request $request): Response
    {

        // je creer une instance d'article
        $article = new Article();
        // methode 3, formType
        // prend le formtype basé sur l'entité (Article)
        // et prend en param une instance d'article
        $form = $this->createForm(ArticleType::class, $article);

        // j'indique que c'est le formulaire qui va gerer la requête
        $form->handleRequest($request);
        // si le form est envoyé (POST) et qu'il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // je complete les champs necessaire
            $article->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime());

                // je recupere le manager
            $entityManager = $this->getDoctrine()->getManager();
            // et j'enregistre
            $entityManager->persist($article);
            $entityManager->flush();
            // et je redirige vers article show
            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        }
        /*
            Methode 2, bien mais pas top
            oblige à dupliquer le code, risque de bug et alourdi la maintenance
            /*
        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, [

                'label' => "Titre de mon article",
                'attr' => [
                    'placeholder' => 'Titre',
                    'class' => '',
                    'required' => true
                ]
            ])
            ->add(
                'content',
                TextareaType::class,
                [
                    'label' => "Contenu de mon article",
                    'attr' => [
                        'class' => '',
                        'required' => true
                    ]
                ]
            )
            ->add(
                'image',
                TextType::class,
                [
                    'label' => "Image",
                    'attr' => [
                        'placeholder' => 'nom de l\'image',
                        'class' => 'form-control',                        
                    ]
                ]
            )
            ->getForm();


        dump($request);
        /*
        methode 1 : à la main fonctionnel, mais fastideuse
        if($request->request->count()>0) {
            $article = new Article();
            $article->setTitle($request->request->get('title'))
                    ->setContent($request->request->get('content'))
                    ->setImage($request->request->get('image'))
                    ->setCreatedAt(new \DateTime())
                    ->setUpdatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        }
        //*/
        return $this->render('article/new.html.twig', [
            'formArticle' => $form->createView(), 
            'btnLabel' => "Ajouter"
        ]);
    }

     /**
     * @Route("/article/{id}/edit", name="article_edit")
     */
    // avec le param converter
    // public function edit(Request $request, Article $article): Response
    // sans param converter
    public function edit(Request $request, $id, ArticleRepository $articleRepository): Response
    {
        $article = $articleRepository->findOneBy(['id' => $id]);
        dump($article);

        // methode 3, formType
        // prend le formtype basé sur l'entité (Article)
        // et prend en param une instance d'article
        $form = $this->createForm(ArticleType::class, $article);

        // j'indique que c'est le formulaire qui va gerer la requête
        $form->handleRequest($request);
        // si le form est envoyé (POST) et qu'il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // je complete les champs necessaire
            $article->setUpdatedAt(new \DateTime());

            // je recupere le manager
            $entityManager = $this->getDoctrine()->getManager();
            // et j'enregistre
            $entityManager->persist($article);
            $entityManager->flush();
            // et je redirige vers article show
            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        }
        
        return $this->render('article/edit.html.twig', [
            'formArticle' => $form->createView(), 
            'article' => $article,
            'btnLabel' => "Mettre à jour"
        ]);
    }

    /**
     * @Route("/article/{id}/delete", name="article_delete")
     */
    // avec le param converter
    // public function edit(Request $request, Article $article): Response
    // sans param converter
    public function delete(Request $request, $id, ArticleRepository $articleRepository): Response
    {
        $article = $articleRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);

        $entityManager->flush();

       return $this->redirectToRoute('article');
    }

}
