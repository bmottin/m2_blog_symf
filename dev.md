## recuperer projet depuis gitlab
- git clone nomDuProjet nomDeLaVersionLocale
    
- cd nomDeLaVersionLocale
- composer install

## projet gitlab
https://gitlab.com/bmottin/m2_blog_symf

## creer une migration
php bin/console make:migration

## migrer la BDD
php bin/console doctrine:migrations:migrate

## Liste des paquets composer
https://packagist.org/

## installer fixtures
composer require orm-fixtures --dev

## vider le cache de symfony
php bin/console cache:clear